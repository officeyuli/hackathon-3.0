package com.androidstudy.hackathon.model

data class MarketItem(val id: String, val name: String, val dollarType: String, val price: Float)

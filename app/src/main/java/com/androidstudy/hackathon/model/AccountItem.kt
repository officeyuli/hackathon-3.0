package com.androidstudy.hackathon.model

data class AccountItem(val id: String, val count: Int, val type: AccountType)

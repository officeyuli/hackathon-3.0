package com.androidstudy.hackathon.model

enum class AccountType {
    GIFT, MONEY
}

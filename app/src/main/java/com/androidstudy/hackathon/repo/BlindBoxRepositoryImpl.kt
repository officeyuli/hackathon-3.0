package com.androidstudy.hackathon.repo

import com.androidstudy.hackathon.model.AccountItem
import com.androidstudy.hackathon.model.MarketItem

class BlindBoxRepositoryImpl : BlindBoxRepository {
    override suspend fun fetchBlindBoxSellingList(): List<String> {
        TODO("Not yet implemented")
    }

    override suspend fun buyBlindBox(targetVersion: String) {
        TODO("Not yet implemented")
    }

    override suspend fun fetchTargetVersionInformation(targetVersion: String): List<String> {
        TODO("Not yet implemented")
    }

    override suspend fun openBlindBox(blindBoxToken: String): String {
        TODO("Not yet implemented")
    }

    override suspend fun fetchMyBlindBoxList(): List<String> {
        TODO("Not yet implemented")
    }

    override suspend fun tradeBlindBox(blindBoxToken: String, targetUser: String) {
        TODO("Not yet implemented")
    }

    override suspend fun fetchAccountListFromServer(): List<AccountItem> {
        TODO("Not yet implemented")
    }

    override suspend fun fetchMarketListFromServer(): List<MarketItem> {
        TODO("Not yet implemented")
    }
}
package com.androidstudy.hackathon.repo

import com.androidstudy.hackathon.model.AccountItem
import com.androidstudy.hackathon.model.AccountType
import com.androidstudy.hackathon.model.MarketItem

class FakeBlindBoxRepositoryImpl : BlindBoxRepository {
    override suspend fun fetchBlindBoxSellingList(): List<String> {
        return listOf("1", "2", "3", "4", "5", "6", "7", "8", "9")
    }

    override suspend fun buyBlindBox(targetVersion: String) {
        //Buy It
    }

    override suspend fun fetchTargetVersionInformation(targetVersion: String): List<String> {
        TODO("Not yet implemented")
    }

    override suspend fun openBlindBox(blindBoxToken: String): String {
        //open it
        return "999"
    }

    override suspend fun fetchMyBlindBoxList(): List<String> {
        return listOf("4", "5", "8", "9")
    }

    override suspend fun tradeBlindBox(blindBoxToken: String, targetUser: String) {
        // trade it
    }

    override suspend fun fetchAccountListFromServer(): List<AccountItem> {
        return listOf(AccountItem("1", 7, AccountType.GIFT), AccountItem("2", 9, AccountType.MONEY))
    }

    override suspend fun fetchMarketListFromServer(): List<MarketItem> {
        val marketItem1 = MarketItem("1", "G1 Blind Box", "USD", 10.5f)
        val marketItem2 = MarketItem("2", "G2 Blind Box", "RAY", 13f)
        val marketItem3 = MarketItem("3", "G3 Blind Box", "USD", 8.3f)
        val marketItem4 = MarketItem("4", "G4 Blind Box", "RAY", 9.0f)
        val marketItem5 = MarketItem("5", "G5 Blind Box", "RAY", 21f)
        val marketItem6 = MarketItem("6", "G6 Blind Box", "RAY", 5.5f)
        val marketItem7 = MarketItem("7", "G7 Blind Box", "USD", 3.3f)
        val marketItem8 = MarketItem("8", "G8 Blind Box", "USD", 4.8f)
        val marketItem9 = MarketItem("9", "G9 Blind Box", "RAY", 12.4f)
        val marketItem10 = MarketItem("10", "G10 Blind Box", "RAY", 20f)


        return listOf(
            marketItem1,
            marketItem2,
            marketItem3,
            marketItem4,
            marketItem5,
            marketItem6,
            marketItem7,
            marketItem8,
            marketItem9,
            marketItem10
        )
    }
}
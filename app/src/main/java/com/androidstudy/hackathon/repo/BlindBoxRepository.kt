package com.androidstudy.hackathon.repo

import com.androidstudy.hackathon.model.AccountItem
import com.androidstudy.hackathon.model.MarketItem

interface BlindBoxRepository {
    suspend fun fetchBlindBoxSellingList(): List<String>
    suspend fun buyBlindBox(targetVersion: String)
    suspend fun fetchTargetVersionInformation(targetVersion: String): List<String>
    suspend fun openBlindBox(blindBoxToken: String): String

    suspend fun fetchMyBlindBoxList(): List<String>
    suspend fun tradeBlindBox(blindBoxToken: String, targetUser: String)
    suspend fun fetchAccountListFromServer(): List<AccountItem>
    suspend fun fetchMarketListFromServer(): List<MarketItem>


}
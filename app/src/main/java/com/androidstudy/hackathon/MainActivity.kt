package com.androidstudy.hackathon

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.androidstudy.hackathon.ui.account.AccountFragment
import com.androidstudy.hackathon.ui.main.MainFragment
import com.androidstudy.hackathon.ui.market.MarketFragment
import com.androidstudy.hackathon.ui.swap.SwapFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MainFragment.newInstance())
                .commitNow()
        }

        findViewById<BottomNavigationView>(R.id.navigation)?.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.action_blindbox -> supportFragmentManager.beginTransaction()
                    .replace(R.id.container, MainFragment.newInstance())
                    .commitNow()
                R.id.action_market -> supportFragmentManager.beginTransaction()
                    .replace(R.id.container, MarketFragment.newInstance())
                    .commitNow()
                R.id.action_swap -> supportFragmentManager.beginTransaction()
                    .replace(R.id.container, SwapFragment.newInstance())
                    .commitNow()
                R.id.action_account -> supportFragmentManager.beginTransaction()
                    .replace(R.id.container, AccountFragment.newInstance())
                    .commitNow()
            }
            true
        }
    }
}
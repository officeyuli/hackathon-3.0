package com.androidstudy.hackathon.ui.account

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.androidstudy.hackathon.model.AccountItem
import com.androidstudy.hackathon.repo.BlindBoxRepository
import com.androidstudy.hackathon.repo.FakeBlindBoxRepositoryImpl
import kotlinx.coroutines.launch

class AccountViewModel : ViewModel() {
    private var blindBoxRepository: BlindBoxRepository = FakeBlindBoxRepositoryImpl()
    private val _accountItemList: MutableLiveData<List<AccountItem>> = MutableLiveData()
    val accountItemList: LiveData<List<AccountItem>> get() = _accountItemList


    fun fetchAccountListFromServer() {
        viewModelScope.launch {
            _accountItemList.value = blindBoxRepository.fetchAccountListFromServer()

        }
    }

}
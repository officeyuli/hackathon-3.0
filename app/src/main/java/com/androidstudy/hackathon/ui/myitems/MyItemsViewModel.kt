package com.androidstudy.hackathon.ui.myitems

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.androidstudy.hackathon.repo.BlindBoxRepository
import com.androidstudy.hackathon.repo.FakeBlindBoxRepositoryImpl
import kotlinx.coroutines.launch

class MyItemsViewModel : ViewModel() {
    private var blindBoxRepository: BlindBoxRepository = FakeBlindBoxRepositoryImpl()
    private val _myItemsList: MutableLiveData<List<String>> = MutableLiveData()
    val myItemsList: LiveData<List<String>> get() = _myItemsList

    fun fetchMyItemsListFromServer() {
        viewModelScope.launch {
            _myItemsList.value = blindBoxRepository.fetchMyBlindBoxList()
        }
    }

}
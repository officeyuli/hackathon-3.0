package com.androidstudy.hackathon.ui.market

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.androidstudy.hackathon.R
import com.androidstudy.hackathon.databinding.FragmentMarketBinding
import com.androidstudy.hackathon.model.MarketItem
import com.androidstudy.hackathon.ui.account.AccountFragment
import com.androidstudy.hackathon.ui.myitems.MyItemFragment

class MarketFragment : Fragment() {
    companion object {
        fun newInstance() = MarketFragment()
    }

    private lateinit var viewModel: MarketViewModel
    private var _binding: FragmentMarketBinding? = null
    private val binding get() = _binding!!
    private lateinit var marketAdapter: MarketAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMarketBinding.inflate(inflater, container, false)
        initView()

        return binding.root
    }

    private fun initView() {
        marketAdapter = MarketAdapter()
        marketAdapter.onMarketItemClick = object : OnMarketItemClick {
            override fun onMarketBuyClick(marketItem: MarketItem) {
                parentFragmentManager.beginTransaction()
                    .replace(R.id.container, AccountFragment.newInstance())
                    .commitNow()
            }

        }
        binding.marketList.adapter = marketAdapter
        binding.marketList.layoutManager = GridLayoutManager(context, 2)


    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[MarketViewModel::class.java]
        viewModel.fetchMarketListFromServer()

    }

    override fun onResume() {
        super.onResume()
        initObserver()
    }

    private fun initObserver() {
        viewModel.marketList.observe(viewLifecycleOwner, {
            marketAdapter.dataList = it
            marketAdapter.notifyDataSetChanged()
        })
    }
}
package com.androidstudy.hackathon.ui.trade

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.androidstudy.hackathon.repo.BlindBoxRepository
import com.androidstudy.hackathon.repo.FakeBlindBoxRepositoryImpl
import kotlinx.coroutines.launch

class TradeViewModel : ViewModel() {
    var targetBlindBox: String? = null
    private var blindBoxRepository: BlindBoxRepository = FakeBlindBoxRepositoryImpl()
    private val _tradeSuccess: MutableLiveData<Boolean> = MutableLiveData()
    val tradeSuccess: LiveData<Boolean> get() = _tradeSuccess


    fun buyTargetBlindBox() {
        viewModelScope.launch {
            if (targetBlindBox != null) {
                blindBoxRepository.tradeBlindBox(targetBlindBox!!, "targetUser")
                _tradeSuccess.value = true
            }
        }
    }


}
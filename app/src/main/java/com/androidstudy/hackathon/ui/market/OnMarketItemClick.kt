package com.androidstudy.hackathon.ui.market

import com.androidstudy.hackathon.model.MarketItem

interface OnMarketItemClick {
    fun onMarketBuyClick(marketItem: MarketItem)
}
package com.androidstudy.hackathon.ui.buy

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.androidstudy.hackathon.R
import com.androidstudy.hackathon.databinding.FragmentBuyBindBoxBinding
import com.androidstudy.hackathon.ui.myitems.MyItemFragment

class BuyBlindBoxFragment : Fragment() {

    companion object {
        fun newInstance() = BuyBlindBoxFragment()
    }
    private lateinit var viewModel: BuyBlindBoxViewModel
    private var _binding: FragmentBuyBindBoxBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentBuyBindBoxBinding.inflate(inflater, container, false)

        initView()
        return binding.root
    }

    private fun initView() {
        binding.btnBuy.setOnClickListener {
            viewModel.buyTargetBlindBox()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[BuyBlindBoxViewModel::class.java]
        viewModel.targetBlindBox = arguments?.getString("targetBlindBox")
        binding.description.text = "這是${viewModel.targetBlindBox}盲盒資料"

    }

    override fun onResume() {
        super.onResume()
        initObserver()
    }

    private fun initObserver() {
        viewModel.buySuccess.observe(viewLifecycleOwner, Observer {
            if(it){
                parentFragmentManager.beginTransaction()
                    .replace(R.id.container, MyItemFragment.newInstance())
                    .commitNow()
            }
        })
    }
}
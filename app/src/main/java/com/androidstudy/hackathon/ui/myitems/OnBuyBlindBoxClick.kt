package com.androidstudy.hackathon.ui.myitems

interface OnMyItemsClick {
    fun onOpenClick(targetBlindBox:String)
    fun onTradeClick(targetBlindBox:String)

}
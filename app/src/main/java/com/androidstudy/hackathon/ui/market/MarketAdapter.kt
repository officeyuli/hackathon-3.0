package com.androidstudy.hackathon.ui.market

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.androidstudy.hackathon.databinding.ItemMarketBinding
import com.androidstudy.hackathon.model.MarketItem

class MarketAdapter : RecyclerView.Adapter<MarketViewHolder>() {
    var dataList: List<MarketItem> = listOf()
    var onMarketItemClick: OnMarketItemClick? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MarketViewHolder {
        val binding =
            ItemMarketBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MarketViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MarketViewHolder, position: Int) {
        holder.bind(dataList[position])
        if (onMarketItemClick != null) {
            holder.onMarketItemClick = onMarketItemClick
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }


}
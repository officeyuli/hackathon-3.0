package com.androidstudy.hackathon.ui.market

import androidx.recyclerview.widget.RecyclerView
import com.androidstudy.hackathon.databinding.ItemMarketBinding
import com.androidstudy.hackathon.model.MarketItem

class MarketViewHolder(private val binding: ItemMarketBinding) :
    RecyclerView.ViewHolder(binding.root) {
    var onMarketItemClick: OnMarketItemClick? = null

    fun bind(item: MarketItem) {
        binding.tvTitle.text = item.name
        binding.tvDollarType.text = item.dollarType
        binding.tvPrice.text = item.price.toString()

        binding.btnBuy.setOnClickListener {
            onMarketItemClick?.onMarketBuyClick(item)
        }
    }
}
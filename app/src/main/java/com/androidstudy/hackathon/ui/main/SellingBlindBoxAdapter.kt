package com.androidstudy.hackathon.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.androidstudy.hackathon.databinding.ItemSellingBlindBoxBinding

class SellingBlindBoxAdapter : RecyclerView.Adapter<SellingBlindBoxViewHolder>() {
    var dataList: List<String> = listOf()
    var onBuyBlindBoxClick: OnBuyBlindBoxClick? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SellingBlindBoxViewHolder {
        val binding =
            ItemSellingBlindBoxBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SellingBlindBoxViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SellingBlindBoxViewHolder, position: Int) {
        holder.bind(dataList[position])
        if (onBuyBlindBoxClick != null) {
            holder.onBuyBlindBoxClickListener = onBuyBlindBoxClick
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }


}
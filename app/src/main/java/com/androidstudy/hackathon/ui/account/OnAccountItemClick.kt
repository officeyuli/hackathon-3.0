package com.androidstudy.hackathon.ui.account

import com.androidstudy.hackathon.model.AccountItem

interface OnAccountItemClick {
    fun onOpenClick(accountItem: AccountItem)
    fun onSellClick(accountItem: AccountItem)
}
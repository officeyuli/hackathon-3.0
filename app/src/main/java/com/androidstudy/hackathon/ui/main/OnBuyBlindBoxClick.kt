package com.androidstudy.hackathon.ui.main

interface OnBuyBlindBoxClick {
    fun onBuyClick(targetBlindBox: String)
}
package com.androidstudy.hackathon.ui.main

import androidx.recyclerview.widget.RecyclerView
import com.androidstudy.hackathon.databinding.ItemSellingBlindBoxBinding

class SellingBlindBoxViewHolder(private val binding: ItemSellingBlindBoxBinding) :
    RecyclerView.ViewHolder(binding.root) {
    var onBuyBlindBoxClickListener: OnBuyBlindBoxClick? = null

    fun bind(target: String) {
//        binding.tvAddress.text = target
        binding.root.setOnClickListener {
            onBuyBlindBoxClickListener?.onBuyClick(target)
        }
    }
}
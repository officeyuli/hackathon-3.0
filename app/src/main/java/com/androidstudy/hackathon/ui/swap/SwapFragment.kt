package com.androidstudy.hackathon.ui.swap

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.androidstudy.hackathon.R
import com.androidstudy.hackathon.databinding.FragmentMainBinding
import com.androidstudy.hackathon.databinding.FragmentSwapBinding
import com.androidstudy.hackathon.ui.main.MainFragment
import com.androidstudy.hackathon.ui.main.MainViewModel
import com.androidstudy.hackathon.ui.main.SellingBlindBoxAdapter
import com.androidstudy.hackathon.ui.myitems.MyItemFragment

class SwapFragment : Fragment() {
    companion object {
        fun newInstance() = SwapFragment()
    }

    private lateinit var viewModel: SwapViewModel
    private var _binding: FragmentSwapBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSwapBinding.inflate(inflater, container, false)
        initView()

        return binding.root
    }

    private fun initView() {
        binding.btnConfirm.setOnClickListener {
            parentFragmentManager.beginTransaction()
                .replace(R.id.container, MainFragment.newInstance())
                .commitNow()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[SwapViewModel::class.java]

    }

    override fun onResume() {
        super.onResume()
        initObserver()
    }

    private fun initObserver() {
    }


}
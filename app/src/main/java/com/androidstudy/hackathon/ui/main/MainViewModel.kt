package com.androidstudy.hackathon.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.androidstudy.hackathon.repo.BlindBoxRepository
import com.androidstudy.hackathon.repo.FakeBlindBoxRepositoryImpl
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {
    private var blindBoxRepository:BlindBoxRepository = FakeBlindBoxRepositoryImpl()
    private val _blindBoxSellingList: MutableLiveData<List<String>> = MutableLiveData()
    val blindBoxSellingList :LiveData<List<String>> get() = _blindBoxSellingList

    fun fetchSellingBlindBoxDataFromServer(){
        viewModelScope.launch {
            _blindBoxSellingList.value = blindBoxRepository.fetchBlindBoxSellingList()
        }
    }

}
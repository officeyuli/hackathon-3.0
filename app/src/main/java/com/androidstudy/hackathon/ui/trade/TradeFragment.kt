package com.androidstudy.hackathon.ui.trade

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.androidstudy.hackathon.R
import com.androidstudy.hackathon.databinding.FragmentBuyBindBoxBinding
import com.androidstudy.hackathon.databinding.FragmentPrizeBinding
import com.androidstudy.hackathon.databinding.FragmentTradeBinding
import com.androidstudy.hackathon.ui.main.MainFragment
import com.androidstudy.hackathon.ui.myitems.MyItemFragment

class TradeFragment : Fragment() {

    companion object {
        fun newInstance() = TradeFragment()
    }

    private lateinit var viewModel: TradeViewModel
    private var _binding: FragmentTradeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTradeBinding.inflate(inflater, container, false)

        initView()
        return binding.root
    }

    private fun initView() {
        binding.btnTrade.setOnClickListener {
            viewModel.buyTargetBlindBox()
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[TradeViewModel::class.java]
        viewModel.targetBlindBox = arguments?.getString("targetBlindBox")

    }

    override fun onResume() {
        super.onResume()
        initObserver()
    }

    private fun initObserver() {
        viewModel.tradeSuccess.observe(viewLifecycleOwner, Observer {
            if (it) {
                parentFragmentManager.beginTransaction()
                    .replace(R.id.container, MainFragment.newInstance())
                    .commitNow()
            }
        })
    }
}
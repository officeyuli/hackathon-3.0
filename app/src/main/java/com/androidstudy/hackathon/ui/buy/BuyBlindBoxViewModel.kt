package com.androidstudy.hackathon.ui.buy

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.androidstudy.hackathon.repo.BlindBoxRepository
import com.androidstudy.hackathon.repo.FakeBlindBoxRepositoryImpl
import kotlinx.coroutines.launch

class BuyBlindBoxViewModel : ViewModel() {
    var targetBlindBox :String?  = null
    private var blindBoxRepository: BlindBoxRepository = FakeBlindBoxRepositoryImpl()
    private val _buySuccess: MutableLiveData<Boolean> = MutableLiveData()
    val buySuccess : LiveData<Boolean> get() = _buySuccess


    fun buyTargetBlindBox() {
        viewModelScope.launch {
            if(targetBlindBox!=null){
                blindBoxRepository.buyBlindBox(targetBlindBox!!)
                _buySuccess.value = true
            }
        }
    }


}
package com.androidstudy.hackathon.ui.account

import androidx.recyclerview.widget.RecyclerView
import com.androidstudy.hackathon.R
import com.androidstudy.hackathon.databinding.ItemAccountBinding
import com.androidstudy.hackathon.databinding.ItemMyItemsBinding
import com.androidstudy.hackathon.model.AccountItem
import com.androidstudy.hackathon.model.AccountType

class AccountViewHolder(private val binding: ItemAccountBinding) :
    RecyclerView.ViewHolder(binding.root) {
    var onAccountItemClick: OnAccountItemClick? = null

    fun bind(item: AccountItem) {
        if (item.type == AccountType.MONEY) {
            binding.imgBlindBoxIcon.setImageResource(R.drawable.account_money_dynamic)
        } else {
            binding.imgBlindBoxIcon.setImageResource(R.drawable.account_gift_dynamic)
        }
        binding.tvCount.text = "x " + item.count + " "

        binding.btnOpen.setOnClickListener {
            onAccountItemClick?.onOpenClick(item)
        }

        binding.btnSell.setOnClickListener {
            onAccountItemClick?.onSellClick(item)
        }
    }
}
package com.androidstudy.hackathon.ui.market

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.androidstudy.hackathon.model.MarketItem
import com.androidstudy.hackathon.repo.BlindBoxRepository
import com.androidstudy.hackathon.repo.FakeBlindBoxRepositoryImpl
import kotlinx.coroutines.launch

class MarketViewModel : ViewModel() {
    private var blindBoxRepository: BlindBoxRepository = FakeBlindBoxRepositoryImpl()
    private val _marketList: MutableLiveData<List<MarketItem>> = MutableLiveData()
    val marketList: LiveData<List<MarketItem>> get() = _marketList

    fun fetchMarketListFromServer() {
        viewModelScope.launch {
            _marketList.value = blindBoxRepository.fetchMarketListFromServer()
        }
    }

}
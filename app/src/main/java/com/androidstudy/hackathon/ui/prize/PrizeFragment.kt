package com.androidstudy.hackathon.ui.prize

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.androidstudy.hackathon.R
import com.androidstudy.hackathon.databinding.FragmentBuyBindBoxBinding
import com.androidstudy.hackathon.databinding.FragmentPrizeBinding
import com.androidstudy.hackathon.ui.main.MainFragment
import com.androidstudy.hackathon.ui.myitems.MyItemFragment

class PrizeFragment : Fragment() {

    companion object {
        fun newInstance() = PrizeFragment()
    }

    private lateinit var viewModel: PrizeViewModel
    private var _binding: FragmentPrizeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPrizeBinding.inflate(inflater, container, false)

        initView()
        return binding.root
    }

    private fun initView() {
        binding.btnExcellent.setOnClickListener {
            parentFragmentManager.beginTransaction()
                .replace(R.id.container, MainFragment.newInstance())
                .commitNow()
        }
        
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[PrizeViewModel::class.java]
        viewModel.targetBlindBox = arguments?.getString("targetBlindBox")
        viewModel.openBlindBox()
    }

    override fun onResume() {
        super.onResume()
        initObserver()
    }

    private fun initObserver() {
        viewModel.openSuccess.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.description.text = "你開到999元"
            }
        })
    }
}
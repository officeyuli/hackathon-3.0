package com.androidstudy.hackathon.ui.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidstudy.hackathon.R
import com.androidstudy.hackathon.databinding.FragmentAccountBinding
import com.androidstudy.hackathon.databinding.FragmentMainBinding
import com.androidstudy.hackathon.model.AccountItem
import com.androidstudy.hackathon.ui.main.MainFragment
import com.androidstudy.hackathon.ui.main.MainViewModel
import com.androidstudy.hackathon.ui.main.SellingBlindBoxAdapter
import com.androidstudy.hackathon.ui.myitems.MyItemFragment
import com.androidstudy.hackathon.ui.myitems.MyItemsAdapter
import com.androidstudy.hackathon.ui.myitems.OnMyItemsClick
import com.androidstudy.hackathon.ui.prize.PrizeFragment
import com.androidstudy.hackathon.ui.swap.SwapFragment
import com.androidstudy.hackathon.ui.trade.TradeFragment

class AccountFragment : Fragment() {
    companion object {
        fun newInstance() = AccountFragment()
    }

    private lateinit var viewModel: AccountViewModel
    private var _binding: FragmentAccountBinding? = null
    private val binding get() = _binding!!
    private lateinit var accountItemsAdapter: AccountItemsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAccountBinding.inflate(inflater, container, false)
        initView()

        return binding.root
    }

    private fun initView() {
        accountItemsAdapter = AccountItemsAdapter()
        accountItemsAdapter.onAccountItemClick = object : OnAccountItemClick {
            override fun onOpenClick(accountItem: AccountItem) {
                TODO("Not yet implemented")
            }

            override fun onSellClick(accountItem: AccountItem) {
                parentFragmentManager.beginTransaction()
                    .replace(R.id.container, SwapFragment.newInstance())
                    .commitNow()
            }
        }
        binding.accountList.adapter = accountItemsAdapter
        binding.accountList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.accountList.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[AccountViewModel::class.java]
        viewModel.fetchAccountListFromServer()

    }

    override fun onResume() {
        super.onResume()
        initObserver()
    }

    private fun initObserver() {
        viewModel.accountItemList.observe(viewLifecycleOwner, Observer {
            accountItemsAdapter.dataList = it
            accountItemsAdapter.notifyDataSetChanged()
        })
    }
}
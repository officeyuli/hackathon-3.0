package com.androidstudy.hackathon.ui.myitems

import androidx.recyclerview.widget.RecyclerView
import com.androidstudy.hackathon.databinding.ItemMyItemsBinding

class MyItemsViewHolder(private val binding: ItemMyItemsBinding) :
    RecyclerView.ViewHolder(binding.root) {
    var onMyItemsClickListener: OnMyItemsClick? = null

    fun bind(target: String) {
        binding.tvAddress.text = target

        binding.btnOpenIt.setOnClickListener {
            onMyItemsClickListener?.onOpenClick(target)
        }

        binding.btnTradeIt.setOnClickListener {
            onMyItemsClickListener?.onTradeClick(target)
        }
    }
}
package com.androidstudy.hackathon.ui.myitems

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.androidstudy.hackathon.databinding.ItemMyItemsBinding
import com.androidstudy.hackathon.databinding.ItemSellingBlindBoxBinding

class MyItemsAdapter : RecyclerView.Adapter<MyItemsViewHolder>() {
    var dataList: List<String> = listOf()
    var onMyItemsClickLister: OnMyItemsClick? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyItemsViewHolder {
        val binding =
            ItemMyItemsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyItemsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyItemsViewHolder, position: Int) {
        holder.bind(dataList[position])
        if (onMyItemsClickLister != null) {
            holder.onMyItemsClickListener = onMyItemsClickLister
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }


}
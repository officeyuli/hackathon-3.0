package com.androidstudy.hackathon.ui.account

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.androidstudy.hackathon.databinding.ItemAccountBinding
import com.androidstudy.hackathon.databinding.ItemMyItemsBinding
import com.androidstudy.hackathon.model.AccountItem

class AccountItemsAdapter : RecyclerView.Adapter<AccountViewHolder>() {
    var dataList: List<AccountItem> = listOf()
    var onAccountItemClick: OnAccountItemClick? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AccountViewHolder {
        val binding =
            ItemAccountBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AccountViewHolder(binding)
    }

    override fun onBindViewHolder(holder: AccountViewHolder, position: Int) {
        holder.bind(dataList[position])
        if (onAccountItemClick != null) {
            holder.onAccountItemClick = onAccountItemClick
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }


}
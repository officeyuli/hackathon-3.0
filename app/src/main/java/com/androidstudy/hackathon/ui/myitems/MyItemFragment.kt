package com.androidstudy.hackathon.ui.myitems

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidstudy.hackathon.R
import com.androidstudy.hackathon.databinding.FragmentMyItemsBinding
import com.androidstudy.hackathon.ui.prize.PrizeFragment
import com.androidstudy.hackathon.ui.trade.TradeFragment

class MyItemFragment : Fragment() {

    companion object {
        fun newInstance() = MyItemFragment()
    }

    private lateinit var viewModel: MyItemsViewModel
    private var _binding: FragmentMyItemsBinding? = null
    private val binding get() = _binding!!
    private lateinit var sellingBlindBoxAdapter: MyItemsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMyItemsBinding.inflate(inflater, container, false)
        initView()

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[MyItemsViewModel::class.java]
        viewModel.fetchMyItemsListFromServer()

    }

    override fun onResume() {
        super.onResume()
        initObserver()
    }

    private fun initView() {
        sellingBlindBoxAdapter = MyItemsAdapter()
        sellingBlindBoxAdapter.onMyItemsClickLister = object : OnMyItemsClick {
            override fun onOpenClick(targetBlindBox: String) {
                val mFrag = PrizeFragment.newInstance()
                val bundle = Bundle()
                bundle.putString("targetBlindBox", targetBlindBox)
                mFrag.arguments = bundle

                parentFragmentManager.beginTransaction()
                    .replace(R.id.container, mFrag)
                    .commitNow()
            }

            override fun onTradeClick(targetBlindBox: String) {
                val mFrag = TradeFragment.newInstance()
                val bundle = Bundle()
                bundle.putString("targetBlindBox", targetBlindBox)
                mFrag.arguments = bundle

                parentFragmentManager.beginTransaction()
                    .replace(R.id.container, mFrag)
                    .commitNow()
            }

        }
        binding.SellingBlindBoxList.adapter = sellingBlindBoxAdapter
        binding.SellingBlindBoxList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)


    }

    private fun initObserver() {
        viewModel.myItemsList.observe(viewLifecycleOwner, Observer {
            sellingBlindBoxAdapter.dataList = it
            sellingBlindBoxAdapter.notifyDataSetChanged()
        })
    }
}
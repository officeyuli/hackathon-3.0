package com.androidstudy.hackathon.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidstudy.hackathon.R
import com.androidstudy.hackathon.databinding.FragmentMainBinding
import com.androidstudy.hackathon.ui.buy.BuyBlindBoxFragment
import com.androidstudy.hackathon.ui.market.MarketFragment

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel
    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!
    private lateinit var sellingBlindBoxAdapter: SellingBlindBoxAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        initView()

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[MainViewModel::class.java]
        viewModel.fetchSellingBlindBoxDataFromServer()

    }

    override fun onResume() {
        super.onResume()
        initObserver()
    }

    private fun initView() {
        sellingBlindBoxAdapter = SellingBlindBoxAdapter()
        sellingBlindBoxAdapter.onBuyBlindBoxClick = object : OnBuyBlindBoxClick {
            override fun onBuyClick(targetBlindBox: String) {
                parentFragmentManager.beginTransaction()
                    .replace(R.id.container, MarketFragment.newInstance())
                    .commitNow()
            }
        }
        binding.SellingBlindBoxList.adapter = sellingBlindBoxAdapter
        binding.SellingBlindBoxList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        binding.btnBuy.setOnClickListener {
            parentFragmentManager.beginTransaction()
                .replace(R.id.container, MarketFragment.newInstance())
                .commitNow()
        }

    }

    private fun initObserver() {
        viewModel.blindBoxSellingList.observe(viewLifecycleOwner, Observer {
            sellingBlindBoxAdapter.dataList = it
            sellingBlindBoxAdapter.notifyDataSetChanged()
        })
    }
}
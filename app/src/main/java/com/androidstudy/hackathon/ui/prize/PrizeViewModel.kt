package com.androidstudy.hackathon.ui.prize

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.androidstudy.hackathon.repo.BlindBoxRepository
import com.androidstudy.hackathon.repo.FakeBlindBoxRepositoryImpl
import kotlinx.coroutines.launch

class PrizeViewModel : ViewModel() {
    var targetBlindBox: String? = null
    private var blindBoxRepository: BlindBoxRepository = FakeBlindBoxRepositoryImpl()
    private val _openSuccess: MutableLiveData<Boolean> = MutableLiveData()
    val openSuccess: LiveData<Boolean> get() = _openSuccess

    fun openBlindBox() {
        viewModelScope.launch {
            if (targetBlindBox != null) {
                blindBoxRepository.openBlindBox(targetBlindBox!!)
                _openSuccess.value = true
            }
        }
    }


}